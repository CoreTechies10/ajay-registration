<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registration </title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="application/javascript">var base_url = "<?php echo base_url(); ?>"</script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <?php echo $this->session->flashdata('verify_msg'); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>User Registration Form</h4>
                </div>
                <div class="panel-body">
                    <form name="registrationform" id="registrationform" method="post" action="">
                    <div class="form-group">
                        <label for="name">First Name</label>
                        <input class="form-control" name="first_name" id="first_name" placeholder="Your First Name" type="text" value="<?php echo set_value('fname'); ?>" />
                        <span class="text-danger" id="alert_first_name"><?php echo form_error('fname'); ?></span>
                    </div>

                    <div class="form-group">
                        <label for="name">Last Name</label>
                        <input class="form-control" name="last_name" id="last_name" placeholder="Last Name" type="text" value="<?php echo set_value('lname'); ?>" />
                        <span class="text-danger" id="alert_last_name"><?php echo form_error('lname'); ?></span>
                    </div>

                    <div class="form-group">
                        <label for="email">Email ID</label>
                        <input class="form-control" name="email" id="email_id" placeholder="Email-ID" type="text" value="<?php echo set_value('email'); ?>" />
                        <span class="text-danger" id="alert_email_id"><?php echo form_error('email'); ?></span>
                    </div>

                    <div class="form-group">
                        <label for="email">Mobile Number</label>
                        <input class="form-control" name="mobile_number" id="mobile_number" placeholder="Mobile-Number" type="text" value="<?php echo set_value('mobile_number'); ?>" />
                        <span class="text-danger" id="alert_mobile_number"><?php echo form_error('mobile_number'); ?></span>
                    </div>

                    <div class="form-group">
                        <label for="subject">Password</label>
                        <input class="form-control" name="password" id="password" placeholder="Password" type="password" />
                        <span class="text-danger" id="alert_password"><?php echo form_error('password'); ?></span>
                    </div>

                    <div class="form-group">
                        <label for="subject">Confirm Password</label>
                        <input class="form-control" name="cpassword" id="confirm_password" placeholder="Confirm Password" type="password" />
                        <span class="text-danger" id="alert_confirm_password"><?php echo form_error('cpassword'); ?></span>
                    </div>

                    <div class="form-group">
                        <button name="submit" type="submit" id="registration_submit" class="btn btn-default" value="submit">Signup</button>
                        <a href="<?php echo base_url(); ?>login" class="btn btn-default">Already Registered</a>
                    </div>
                    </form>
                    <?php echo $this->session->flashdata('msg'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

<script type="application/javascript">
    $(document).ready(function() {
        $('#registration_submit').click(function(e) {
            var checkFocus=0;
        // Initializing Variables With Form Element Values
            var firstname = $('#first_name').val();
            var lastname = $('#last_name').val();
            var emailaddress = $('#email_id').val();
            var mobilenumber = $('#mobile_number').val();
            var password = $('#password').val();
            var confirmpassword = $('#confirm_password').val();
            // Initializing Variables With Regular Expressions
            var name_regex = /^[a-zA-Z]+$/;
            var email_regex = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
            var add_regex = /^[0-9a-zA-Z]+$/;
            var zip_regex = /^[0-9]+$/;
            // First name validation.
             if (firstname.length == 0) {
                $('#alert_first_name').text("You can't leave this empty."); // This Segment Displays The Validation Rule For All Fields
                 if(checkFocus==0){ checkFocus=1; }
                $("#first_name").focus();
             }else if (!firstname.match(name_regex)) {
                $('#alert_first_name').text("Please use only letters (a-z)."); // This Segment Displays The Validation Rule For Name
                 if(checkFocus==0){ checkFocus=1; }
                $("#first_name").focus();
             }else{ $('#alert_first_name').text("");  }

            // Last name validation.
            if (lastname.length == 0) {
                $('#alert_last_name').text("You can't leave this empty."); // This Segment Displays The Validation Rule For All Fields
                if(checkFocus==0){ checkFocus=1; }
                $("#last_name").focus();
            }else if (!lastname.match(name_regex)) {
                $('#alert_last_name').text(" Please use only letters (a-z)."); // This Segment Displays The Validation Rule For Name
                if(checkFocus==0){ checkFocus=1; }
                $("#last_name").focus();
            }else{ $('#alert_last_name').text("");  }

            // Email  validation.
            if (emailaddress.length == 0) {
                $('#alert_email_id').text("You can't leave this empty."); // This Segment Displays The Validation Rule For All Fields
                if(checkFocus==0){ checkFocus=1; }
                $("#email_id").focus();
            }else if (!emailaddress.match(email_regex)) {
                $('#alert_email_id').text("Your email address is invalid. Please enter a valid address."); // This Segment Displays The Validation Rule For Name
                if(checkFocus==0){ checkFocus=1; }
                $("#email_id").focus();
            }else{
                //check email already exits
                $.ajax({
                    type: "POST",
                    url: base_url + "registration_controller/checkEmailExitsValidation",
                    data: {email_address: emailaddress},
                    dataType: "json",
                    success: function (result) {
                        if (result['status'] == true) {
                            $('#alert_email_id').text(" That username is taken. Try another."); // This Segment Displays The Validation Rule For Name
                            if(checkFocus==0){ checkFocus=1; }
                            $("#email_id").focus();
                        }else{
                            $('#alert_email_id').text("");
                        }
                    }
                });
            }
            // Number  validation.
            if (mobilenumber.length == 0) {
                $('#alert_mobile_number').text("You can't leave this empty."); // This Segment Displays The Validation Rule For All Fields
                if(checkFocus==0){ checkFocus=1; }
                $("#mobile_number").focus();
            }else if (!mobilenumber.match(zip_regex)) {
                $('#alert_mobile_number').text("Please use only numbers (0-9)."); // This Segment Displays The Validation Rule For Name
                if(checkFocus==0){ checkFocus=1; }
                $("#mobile_number").focus();
            }else{ $('#alert_mobile_number').text("");  }

            // Password  validation.
            if (password.length == 0) {
                $('#alert_password').text("You can't leave this empty."); // This Segment Displays The Validation Rule For All Fields
                if(checkFocus==0){ checkFocus=1; }
                $("#password").focus();
            }else if (password.length < 6) {
                $('#alert_password').text("Short passwords are easy to guess. Try one with at least 6 characters."); // This Segment Displays The Validation Rule For Name
                if(checkFocus==0){ checkFocus=1; }
                $("#password").focus();
            }else{ $('#alert_password').text("");  }

            // Confirm Password  validation.
            if (confirmpassword.length == 0) {
                $('#alert_confirm_password').text(" You can't leave this empty."); // This Segment Displays The Validation Rule For All Fields
                if(checkFocus==0){ checkFocus=1; }
                $("#confirm_password").focus();
            }else if (password != confirmpassword) {
                $('#alert_confirm_password').text(" These passwords don't match. Try again?"); // This Segment Displays The Validation Rule For Name
                if(checkFocus==0){ checkFocus=1; }
                $("#confirm_password").focus();
            }else{ $('#alert_confirm_password').text("");  }

             if(checkFocus==0){
                return true;
             }else{
                 return false;
             }
        });
    });
</script>