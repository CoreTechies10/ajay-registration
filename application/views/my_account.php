<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My Account</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="application/javascript">var base_url = "<?php echo base_url(); ?>"</script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>User My Account</h4>
                </div>
                <div class="panel-body">
                    <!--==========error message show=================-->
                    <?php if(!empty($response_message)){ echo $response_message; } ?>
                    <!--=============================================-->
                    <table>
                        <tr>
                            <th>Name</th>
                            <td><?php echo $this->session->userdata('memberLoginDetails')['memberLoginFname']." ".$this->session->userdata('memberLoginDetails')['memberLoginLname']; ?></td>
                        </tr>
                        <tr>
                            <th>Contact Email</th>
                            <td><?php echo $this->session->userdata('memberLoginDetails')['memberLoginEmail']; ?></td>
                        </tr>
                        <tr>
                            <th>Contact Number</th>
                            <td><?php echo $this->session->userdata('memberLoginDetails')['memberLoginNumber']; ?></td>
                        </tr>
                        <tr>
                            <th>Login IP</th>
                            <td><?php echo $this->session->userdata('memberLoginDetails')['memberLoginIP']; ?></td>
                        </tr>
                        <tr>
                            <td colspan="2"><a href="<?php echo base_url(); ?>logout" class="btn btn-danger">Logout</a> </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

