<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login </title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="application/javascript">var base_url = "<?php echo base_url(); ?>"</script>
</head>
<body>
<div class="container">

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>User Login Form</h4>
                </div>
                <div class="panel-body">
                    <!--==========error message show=================-->
                    <?php if(!empty($response_message)){ echo $response_message; } ?>
                    <!--=============================================-->
                    <form name="loginform" id="loginform" method="post" action="">
                    <div class="form-group">
                        <label for="email">Email ID</label>
                        <input class="form-control" name="email" id="email_id" placeholder="Email-ID" type="text" value="<?php echo set_value('email'); ?>" />
                        <span class="text-danger" id="alert_email_id"><?php echo form_error('email'); ?></span>
                    </div>

                    <div class="form-group">
                        <label for="subject">Password</label>
                        <input class="form-control" name="password" id="password" placeholder="Password" type="password" />
                        <span class="text-danger" id="alert_password"><?php echo form_error('password'); ?></span>
                    </div>

                    <div class="form-group">
                        <button name="submit" type="submit" id="login_submit" class="btn btn-default" value="submit">Login</button>
                        <a href="<?php echo base_url(); ?>registration" class="btn btn-default">Create an Account</a>
                    </div>
                    </form>
                    <?php echo $this->session->flashdata('msg'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

<script type="application/javascript">
    $(document).ready(function() {
        $('#login_submit').click(function(e) {
            var checkFocus=0;
        // Initializing Variables With Form Element Values
            var emailaddress = $('#email_id').val();
            var password = $('#password').val();
            // Initializing Variables With Regular Expressions
            var name_regex = /^[a-zA-Z]+$/;
            var email_regex = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
            var add_regex = /^[0-9a-zA-Z]+$/;
            var zip_regex = /^[0-9]+$/;
            // Email  validation.
            if (emailaddress.length == 0) {
                $('#alert_email_id').text("You can't leave this empty."); // This Segment Displays The Validation Rule For All Fields
                if(checkFocus==0){ checkFocus=1; }
                $("#email_id").focus();
            }else if (!emailaddress.match(email_regex)) {
                $('#alert_email_id').text("Your email address is invalid. Please enter a valid address."); // This Segment Displays The Validation Rule For Name
                if(checkFocus==0){ checkFocus=1; }
                $("#email_id").focus();
            }else{$('#alert_email_id').text("");}

            // Password  validation.
            if (password.length == 0) {
                $('#alert_password').text("You can't leave this empty."); // This Segment Displays The Validation Rule For All Fields
                if(checkFocus==0){ checkFocus=1; }
                $("#password").focus();
            }else if (password.length < 6) {
                $('#alert_password').text("Short passwords are easy to guess. Try one with at least 6 characters."); // This Segment Displays The Validation Rule For Name
                if(checkFocus==0){ checkFocus=1; }
                $("#password").focus();
            }else{ $('#alert_password').text("");  }

             if(checkFocus==0){
                return true;
             }else{
                 return false;
             }
        });
    });
</script>