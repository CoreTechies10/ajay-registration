<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form','url'));
        $this->load->library(array('session', 'form_validation', 'email'));
        $this->load->database();
        $this->load->model('registration_module');
    }
    //this function use for check user already login
    public function isUserLogin()
    {
        //$this->session->userdata('memberLoginDetails')['memberRegisterID'];
        if (!$this->session->userdata('memberLoginDetails')) {
            redirect(base_url()."login");
        }
    }
    //this function use for member already login then redirect to my account page
    public function isUserAlreadyLogin()
    {
        //$this->session->userdata('memberLoginDetails')['memberRegisterID'];
        if ($this->session->userdata('memberLoginDetails')) {
            redirect(base_url()."myaccount");
        }
    }
}
