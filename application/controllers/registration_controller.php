<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registration_controller extends MY_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 */
	public function __construct()
	{
		parent::__construct();
	}
	//==================this is index function===============//
	function index()
	{
		$this->isUserAlreadyLogin();
		if($this->input->post('submit')){
			//set validation rules
			$this->form_validation->set_rules('first_name', 'First Name', 'trim|required|alpha|min_length[3]|max_length[30]|xss_clean');
			$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|alpha|min_length[3]|max_length[30]|xss_clean');
			$this->form_validation->set_rules('email', 'Email ID', 'trim|required|valid_email|is_unique[user.email]');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|md5');
			$this->form_validation->set_rules('cpassword', 'Confirm Password', 'trim|required|matches[password]|md5');
			$this->form_validation->set_rules('mobile_number', 'Mobile Number', 'trim|required|num');
			//validate form input
			if ($this->form_validation->run() == TRUE)
			{
				$email_verification=md5($this->input->post('email').date('Y-m-d H:i:s'));
				//insert the user registration details into database
				$data = array(
					'fname' => $this->input->post('first_name'),
					'lname' => $this->input->post('last_name'),
					'email' => $this->input->post('email'),
					'password' => $this->input->post('password'),
					'mobile_number' => $this->input->post('mobile_number'),
					'join_date'=>date('Y-m-d H:i:s'),
					'user_block'=>'N',
					'account_activate'=>'N',
					'verfication_code'=>$email_verification
				);

				// insert form data into database
				if ($this->registration_module->insertUser($data))
				{
					//this working for send account vefication link
					//==send variable details==//
					$send_email_address=$this->input->post('email');
					$email_activation_link=base_url().'accout_activate/'.$email_verification;
					//===========message mange===========//
					$email_design='';
					$email_design.='<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1.0">';
					$email_design.='<title>WTF</title><link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous"></head>';
					$email_design.='<body  style="background:#f4f4f4; height:100%; font-family: sans-serif;"><div style="width:600px; margin:0 auto; background:#fff; padding:15px; border:1px solid #E1E1E1;">';
					$email_design.='<div  style=" width:550px; padding:25px ; margin:0 auto;  background:#fff;  border-bottom:1px solid #E1E1E1;">';
					$email_design.='Demo Registration</div>';
					$email_design.='<div class="thanks_container" style=" width:550px; background:#fff;  padding:25px; margin:0 auto;">';
					$email_design.='<div  style="color:#464646 !important; padding:20px 50px; text-align:center;">';
					$email_design.='<h1 style="font-weight:normal; font-size:25px; color:inherit; margin-bottom:20px;"> Thanks for Registration. </h1>';
					$email_design.='<h1 style="font-weight:normal; font-size:20px; color:#6F6F6F; line-height:30px;"> Active your account by clicking on the link below and get ready to showcase your work to the word. </h1>';
					$email_design.='</div>';
					$email_design.='<div class="text-center" style="color:#ADADAD !important; padding: 25px 50px 0px;margin-left: -25px; margin-right: -25px; border-top:1px solid #dedede;">';
					$email_design.='<span style="font-size:20px;"><i class="fa fa-check" style=" margin-right:20px; color:#3C0; font-size:40px; vertical-align:middle;"></i>';
					$email_design.='<a href="'.$email_activation_link.'" style="font-size:inherit; color:#009EDF; text-decoration:none; font-weight:600;">Click Here</a> to activate your account</span>';
					$email_design.='</div>';
					$message_output=$email_design;
					//=======================//
					$emailToList[]=$send_email_address;
					$emailCCList=array();
					$emailBCCList=array();
					$message=$message_output;
					$subject="Registration";
					$status= sendmail($emailToList,$emailCCList,$emailBCCList,$message,$subject);

					$this->session->set_flashdata('response_message','registration_success');
					redirect(base_url().'login');
				}
				else
				{
					// error
					$this->session->set_flashdata('msg','<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
					redirect('registration');
				}
			}
		}
		$this->load->view("registration");
	}
	/**
	 * this function use for email already exits yes or not for validation
	 */
	function checkEmailExitsValidation()
	{
		header('Content-type: application/json');
		$return_message = array(
			'status' => false
		);
		$email_address=$_POST['email_address'];
		$filed_exits='id';
		$email_where=array('email'=>$email_address);
		$email_exits=$this->registration_module->checkDetailsExitsYesorNot('user',$filed_exits,$email_where);
		if($email_exits>0){
			$return_message = array(
				'status' => true
			);
		}
		echo json_encode($return_message);
	}
	// this is login function
	function login()
	{
		$this->isUserAlreadyLogin();
		$data['response_message']='';
		//get login error and set login message
		if($this->session->flashdata('response_message')){
			if($this->session->flashdata('response_message')=='account_block'){
				$data['response_message']="<div class='alert alert-danger'>your account inactive please active your account.</div>";
			}else if($this->session->flashdata('response_message')=='logout_success'){
				$data['response_message']="<div class='alert alert-success'>your account logout successfully.</div>";
			}else if($this->session->flashdata('response_message')=='registration_success'){
				$data['response_message']="<div class='alert alert-success'>You are Successfully Registered! Please confirm the mail sent to your Email-ID!!!</div>";
			}else if($this->session->flashdata('response_message')=='link_invalid'){
				$data['response_message']="<div class='alert alert-success'>Sorry your activation link invalid please click valid link.</div>";
			}else{
				$data['response_message']="<div class='alert alert-danger'>Your login details incorrect please enter correct login details.</div>";
			}
		}
		if($this->input->post('submit')){
				$this->form_validation->set_rules('email', 'Email ID', 'trim|required|valid_email');
				$this->form_validation->set_rules('password', 'Password', 'trim|required');
				//validate form input
				if ($this->form_validation->run() == TRUE)
				{
					$ip_address = $this->input->ip_address();
					$email_address=$this->input->post('email');
					$password=md5($this->input->post('password'));
					//get login user full details
					$login_fild='id,fname,lname,email,mobile_number,user_block,account_activate';
					$login_condition=array('email'=>$email_address,'password'=>$password);
					$login_details=$this->registration_module->getFullDescription('user',$login_fild,$login_condition);
					//check user active or not
					if(!empty($login_details)){
						if($login_details->account_activate=='Y'){
							$login_session_data = array(
								'memberLoginID' => $login_details->id,
								'memberLoginFname' => $login_details->fname,
								'memberLoginLname' => $login_details->lname,
								'memberLoginEmail' => $login_details->email,
								'memberLoginNumber' => $login_details->mobile_number,
								'memberLoginIP' =>$ip_address
							);
							//Set login session
							$this->session->set_userdata('memberLoginDetails', $login_session_data);
							//Update last login ip address
							$update_data=array(
								'last_login_ip'=>$ip_address
							);
							$update_where=array('id'=>$login_details->id);
							$this->registration_module->updateDetails('user',$update_data,$update_where);
							redirect(base_url()."myaccount");
						}else{
							$this->session->set_flashdata('response_message', 'account_block');
							redirect(base_url()."login");
						}
					}else{
						$this->session->set_flashdata('response_message', 'login_failed');
						redirect(base_url()."login");
					}
				}
		}
		$this->load->view("login",$data);
	}
	//this function use for after login user my account
	function myaccount()
	{
		$this->isUserLogin();
		$data['response_message']='';
		//get login error and set login message
		if($this->session->flashdata('response_message')){
			if($this->session->flashdata('response_message')=='login_success'){
				$data['response_message']='<div class="alert alert-success">Successfully login your account.</div>';
			}else if($this->session->flashdata('response_message')=='account_active'){
				$data['response_message']='<div class="alert alert-success">Successfully active your account.</div>';
			}
		}
		$this->load->view("my_account",$data);
	}
	//this function use for logout user account
	function logout()
	{
		$this->session->set_flashdata('response_message', 'logout_success');
		$this->session->unset_userdata('memberLoginDetails');
		redirect(base_url()."login");
	}
	//this function use for account activate using by email link
	function accout_activate()
	{
		if ($this->uri->segment(2)) {
			$ip_address = $this->input->ip_address();
			$where_condition = array('verfication_code' => $this->uri->segment(2));
			$updae_data = array('account_activate' => 'Y');
			$update_details = $this->registration_module->updateDetails('user', $updae_data, $where_condition);
			if ($update_details) {
				//get login user full details
				$login_fild = 'id,fname,lname,email,mobile_number,user_block,account_activate';
				$login_condition = array('verfication_code' => $this->uri->segment(2));
				$login_details = $this->registration_module->getFullDescription('user', $login_fild, $login_condition);
				//check user active or not
				if (!empty($login_details)) {
					if ($login_details->account_activate == 'Y') {
						$login_session_data = array(
							'memberLoginID' => $login_details->id,
							'memberLoginFname' => $login_details->fname,
							'memberLoginLname' => $login_details->lname,
							'memberLoginEmail' => $login_details->email,
							'memberLoginNumber' => $login_details->mobile_number,
							'memberLoginIP' => $ip_address
						);
						//Set login session
						$this->session->set_userdata('memberLoginDetails', $login_session_data);
						//Update last login ip address
						$update_data = array(
							'last_login_ip' => $ip_address
						);
						$update_where = array('id' => $login_details->id);
						$this->registration_module->updateDetails('user', $update_data, $update_where);
						$this->session->set_flashdata('response_message', 'account_active');
						redirect(base_url() . "myaccount");
					}
				} else {
					$this->session->set_flashdata('response_message', 'account_block');
					redirect(base_url()."login");
				}
			} else {
				$this->session->set_flashdata('response_message', 'link_invalid');
				redirect(base_url()."login");
			}
		}else {
			$this->session->set_flashdata('response_message', 'link_invalid');
			redirect(base_url()."login");
		}
	}
}