   <?php
   function getRupeesConvertInCross($price)
    {
        //$price=31496500;
        //$length=strlen($price);
        $new_array_value = str_split($price, 1);
        $first_value=$new_array_value[0];
        $secound_value=$new_array_value[1];
        $third_value=$new_array_value[2];
        $secound_third=$secound_value.$third_value;
        if($secound_third>0)
        {
            $final_sec_value=$secound_third;
            $four_value=$new_array_value[3];
            $five_value=$new_array_value[4];
            $four_five=$four_value.$five_value;
            if($four_five>5){
                $final_sec_value=$final_sec_value+1;}
        }
        else
        {
            $final_sec_value='00';
        }
        $final_return_value=$first_value.'.'.$final_sec_value." Cr";

        return $final_return_value;
    
    }
   //=================this function use for new entry in chart table for graph
   function newEntryInChartTable($location,$project_id,$flat_num)
   {
       $today_date=date('Y-m-d');
       $ci= & get_instance();
       $ci->load->model('manage_avaible');
       $select_fild = 'ch_id';
       $login_data =  $ci->session->userdata('login_user');
       $user_id = $login_data['id'];
       $where_condition = array(
           'user_id' =>  $user_id,
           'location_name' => $location,
           'download_date' => $today_date,
           'project_id' => $project_id,
           'flat_num' => $flat_num);
       $data_exits = $ci->manage_avaible->checkDetailsExitsYesorNot('chart_table', $select_fild, $where_condition);
       //insert data
       if($data_exits<1){
           $insertdata=array(
               'user_id'=> $user_id,
               'location_name'=>$location,
               'download_date'=>$today_date,
               'project_id' => $project_id,
               'flat_num' => $flat_num
           );
           $ci->manage_avaible->insertDetails('chart_table',$insertdata);

       }
   }

   function getTotalDataForTodayChart($location)
   {
       $today_date=date('Y-m-d');
       $ci= & get_instance();
       $select_fild = 'ch_id';
       $where_condition = array(
           'location_name' => $location,
           'download_date' => $today_date);
       $count_record = $ci->AdminModel->getCountRecord('chart_table',$where_condition);
       return $count_record;
   }
   function getTotalDataForChart($location)
   {
       $ci= & get_instance();
       $select_fild = 'ch_id';
       $where_condition = array(
           'location_name' => $location
       );
       $count_record = $ci->AdminModel->getCountRecord('chart_table',$where_condition);
       return $count_record;
   }
   function getTotal_falt($table_name)
   {
       //$this->load->model('AdminModel');
       $ci= & get_instance();
       $ci->load->model('AdminModel');
       //$select_fild = 'ch_id';
       $where_condition = array(
           'available' => 'yes'
       );
       $count_record = $ci->AdminModel->total_falt_count($table_name,$where_condition);
       return $count_record;
   }
    ?>